(ns rhg135.middleware)

(defn- manage
  [state]
  (update-in state [::current] pop))

(defn wrap-manage
  "Ensures the state is current
  
  atm it pops one off ::current"
  [rst]
  (fn [state & args]
    (apply rst (manage state) args)))

(defn stacked-fn
  "Takes a fn and a middleware stack to use and returns a new function"
  [root stack]
  (reduce (fn [rst f]
            (f rst))
          root
          stack))

(defn managed-stack
  "Takes a middleware stack and adds managed state"
  [stack]
  (->> (or stack [])
       (interpose wrap-manage)
       vec))
