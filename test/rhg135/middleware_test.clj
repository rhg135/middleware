(ns rhg135.middleware-test
  (:require [clojure.test :refer :all]
            [rhg135.middleware :refer :all]))

(deftest stacks
  (testing "identities"
    (are [i n] (= i ((stacked-fn identity (repeat n identity)) i))
         0 0
         1 1
         2 2))
  (testing "empty stack"
    (is (= + (stacked-fn + [])))))

(deftest managed-stacks
  (testing "nil"
    (is (= [] (managed-stack nil))))
  (testing "empty"
    (is (= [] (managed-stack []))))
  (testing "identities"
    (are [n] (= (+ n (- n 1)) (count (managed-stack (vec (repeat n identity)))))
         1
         2
         3
         4
         998
         997)))
